#!/usr/bin/python
#- * - encoding: utf-8 - * -
import email,mimetypes
import string, sys, os
import time
import ConfigParser
import smtplib
import glob

from email import encoders
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from optparse import OptionParser

import smtpser

def ReadConfig(filename):
   """ read param from config file: filename """
   rc=ConfigParser.ConfigParser()
   rc.read(filename)
   maxlen=rc.getint('global','maillen')*1024
   smtpcount=rc.getint('global','smtp')
   defaultsmtp=rc.get('global','default')
   #only use default or use each smtp orderly
   onlydefault=rc.getint('global','onlydefault')
   tolist=rc.get('global','to').split(',')
   param=[]
   servernamelist=[]
   param.append([maxlen,smtpcount,defaultsmtp,onlydefault,tolist])
   smpts={}
   for i in xrange(1,smtpcount+1):
      sname='smtp'+'%d'%i
      key=rc.get(sname,'name')
      addr=rc.get(sname,'addr')
      port=rc.getint(sname,'port')
      ssl=rc.getint(sname,'ssl')
      username=rc.get(sname,'user')
      password=rc.get(sname,'pass')
      mailaddress=rc.get(sname,'mail')
      smpts[key]=[addr,port,ssl,username,password,mailaddress]
      servernamelist.append(key)
   param.append(smpts)
   param.append(servernamelist)

   return param

def GetOption():
   """ get option from command line """
   parser = OptionParser(usage="""\
Send the contents of a directory as a MIME message.

Usage: %prog [options] [directory]

""")
   parser.add_option('-s', '--subject',
                      type='string', action='store', metavar='SUBJECT',
                      help='The value of the Subject: header (required)')
   parser.add_option('-r', '--recipient',
                      type='string', action='append', metavar='RECIPIENT',
                      default=[], dest='recipients',
                      help='A To: header value (at least one required)')
   parser.add_option('', '--bcc',
                      type='string', action='append', metavar='BCC',
                      default=[], dest='bccs',
                      help='A BCC: send by BCC')
   parser.add_option('-f', '--file',
                      type='string', action='append', metavar='FILE',
                      default=[], dest='files',
                      help="A File point out the files to be mailed. Example: '*.jpg' , '../*.pdf' etc. DON NOT TO FORGET THE '. If this option , we will discard directory argument")
   parser.add_option('-c','--configfile',
                     type='string',action='store',metavar='CONFIGFILE',
                     help="The configfile, for Linux is '~/.xmailrc'")
   parser.add_option('-l','--listserver',
                     action='store_true',dest='listserver',
                     help="Only list the server in configfile")
   parser.add_option('', '--maxmaillen',
                      type='float', action='store', metavar='MAXMAILLEN',
                      help='The value of max length of each mail (M)')

   opts, args = parser.parse_args()

   return opts,args

def ReadAlias(filename):
   """ read alias from filename """
   aliasdict={}
   afile=open(filename,'r')
   s=afile.readline().strip()
   while s:
      name,email=s.split(':')
      aliasdict[name]=email.split(',')
      s=afile.readline().strip()
   return aliasdict

def MailAddressFromAlias(alist,adict):
   """ realias the address alist from adict,
 alist: list with alias
 adict: dictionary of alias """
   elist=[]
   for i in alist:
      if adict.has_key(i):
         elist.extend(adict[i])
      else:
         elist.append(i)
   return elist

def NextServ(cur,lists):
   """ get the Next of cur in lists """
   keylen=len(lists)
   for i in range(0,keylen):
      if lists[i]==cur:
         break
   if i==keylen-1:
      i=0
   else:
      i=i+1
   return lists[i]

opts,args=GetOption()

if args :
   direction=args[0]
else:
   direction='.'

if opts.configfile:
   rcfile=opts.configfile
else:
   if sys.platform=='win32':
      rcfile=os.getcwd()+'\\xmail.ini'
      aliasfile=os.getcwd()+'\\xmail.alias'
      charset='gb2312'
   else:
      rcfile=os.environ['HOME']+'/.xmailrc'
      aliasfile=os.environ['HOME']+'/.xmailalias'
      charset='utf-8'
#print rcfile
param=ReadConfig(rcfile)
#print param
glparam=param[0]
# server dict
serdict=param[1]
# server name list
snamelist=param[2]
maxmaillen=glparam[0] # max mail length
maxsmtpser=glparam[1] # max smtp server
defaultsmtp=glparam[2] # default smtp server
onlydefault=glparam[3] # only use default smtp server?
tolist=glparam[4] # to list

mailalias=ReadAlias(aliasfile)

if opts.maxmaillen:
   maxmaillen=int(opts.maxmaillen*1024*1024)
print 'Length for each mail is less then ',maxmaillen
if opts.listserver:
   sender=smtpser.SMTPServer()
   for i in serdict:
      sender.SetInfoFromList(serdict[i])
      print i,sender
   exit()
if opts.recipients: # 如果命令行指定了dest e-mail address
   tolist=opts.recipients # send to lists
if not tolist:
   print 'use -r to add recipients'
   quit()
tolist=MailAddressFromAlias(tolist,mailalias)
if opts.bccs: # add bcc
   bcclist=opts.bccs
   bcclist=MailAddressFromAlias(bcclist,mailalias)
else:
   bcclist=None

if opts.subject:
   subj=str(email.Header.Header(opts.subject,charset))
else:
   subj='X-Mailer: send directory'

sender=smtpser.SMTPServer()
msender=smtpser.MailSenders(serdict,defaultsmtp)
msender.OnlyDefault(onlydefault)
msender.SetSubject(subj,charset)
msender.SetCharset(charset)

print '-'*60
print 'X-Mailer , mail total directory to dest e-mail address'
#print 'Directory:'+direction
print 'To:'+' '.join(tolist)
if bcclist:
   print 'BCC:'+' '.join(bcclist)
print 'use %d SMTP server(s):'%len(serdict),serdict.keys()
print '='*60

files=[]
fileslen=0
cursmtp=defaultsmtp # current used smtp server
      
# get file list
if opts.files:
   fileslist=[]
   for fl in opts.files:
      a=glob.glob(fl)
      fileslist=fileslist+a
else:
   fileslist=glob.glob(direction+'/*') # files in directory
fileslist.sort()  # sort it
print 'We will mail %d file(s):'%len(fileslist)+' '.join(fileslist)

# the sender smtp server
sender.SetInfoFromList(serdict[cursmtp])

for filename in fileslist:
   path=filename #os.path.join(direction,filename)
   if not os.path.isfile(path):
      continue
   filelen=os.stat(path).st_size
   if filelen > maxmaillen : # one big file
      print 'Big file:'+filename
#      print 'From:'+sender.email
#      print 'Subject:'+subj
      bodymsg=msender.CreateBodyMail([filename,'<a href=mailto:r01ustc+xmailer@gmail.com>Contact me</a>'],charset)
      msender.MailBigFile(tolist,subj,bodymsg,path,maxmaillen,charset=charset,bcc=bcclist)
#      sender.MailBigFile(tolist,subj,[filename,'<a href=mailto:r01ustc+xmailer@gmail.com>联系我</a>'],path,maxmaillen,str(email.Header.Header(os.path.basename(filename),charset)))
#      if not onlydefault:
#         cursmtp=NextServ(cursmtp,serdict.keys())
#         sender.SetInfoFromList(serdict[cursmtp])
   else:
      if fileslen+filelen > maxmaillen : # 加上新文件后就超大了
         print 'Mail Files:'+' '.join(files)
#         print 'From:'+sender.email
#         print 'Subject:'+subj
         bodymsg=msender.CreateBodyMail(['\n'.join(files),'<a href=mailto:r01ustc+xmailer@gmail.com>Contact me</a>'],charset)
         msender.MailFiles(tolist,subj,bodymsg,files,charset=charset,bcc=bcclist)
#         sender.MailFiles(tolist,subj,['\n'.join(files),'<a href=mailto:r01ustc+xmailer@gmail.com>联系我</a>'],files,charset)
#         if not onlydefault:
#            cursmtp=NextServ(cursmtp,serdict.keys())
#            sender.SetInfoFromList(serdict[cursmtp])
         files=[path]
         fileslen=filelen
      else: # add new file name to list
         files.append(path)
         fileslen=fileslen+filelen
if files: # has some file not mailed
   print 'Mail Files remained :'+' '.join(files)
#   print 'From:'+sender.email
#   print 'Subject:'+subj
   bodymsg=msender.CreateBodyMail(['\n'.join(files),'<a href=mailto:r01ustc+xmailer@gmail.com>Contact me</a>'],charset)
   msender.MailFiles(tolist,subj,bodymsg,files,charset=charset,bcc=bcclist)
#   sender.MailFiles(tolist,subj,['\n'.join(files),'<a href=mailto:r01ustc+xmailer@gmail.com>联系我</a>'],files,charset)

