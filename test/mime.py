# -*- coding:cp936 -*-
#!/usr/bin/env python
"""
MIME添加附件
1.判断附件的类型guess_type返回类型和编码
2.如果是文本创建MIMEText，否则创建MIMEBase
3.创建MIMEMultipart
4.使用attach向MIMEMultipart中追加对象
"""
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Utils,Encoders
import mimetypes,sys

def attachment(filename):
    fd = file(filename,"rb")
    mimetype,mimeencoding = mimetypes.guess_type(filename)
    if mimeencoding or (mimetype is None):
        mimetype = "application/octet-stream"
    maintype,subtype = mimetype.split("/")
    
    if maintype == "text":
        retval = MIMEText(fd.read(),_subtype = subtype)
    else:
        # 如果不是text
        retval = MIMEBase(maintype,subtype)
        retval.set_payload(fd.read())
        Encoders.encode_base64(retval)
    retval.add_header("Content-Disposition","attachment",filename = filename)
    fd.close()
    return retval

message = """
hello,MIME!
    ---jcodeer
"""
# 创建MIME，并添加信息头
msg = MIMEMultipart()
msg["To"] = "jcodeer@sina.com"
msg["From"]="jcodeer@126.com"
msg["Subject"] = "Hello MIME"
msg["Date"] = Utils.formatdate(localtime = 1)

# 创建MIMEText，并添加到msg
body = MIMEText(message,_subtype = "plain")
msg.attach(body)
# 创建附件，并添加到msg
fname=raw_input('input file to attach:')
msg.attach(attachment(fname))

print msg.as_string()
