import os
import sys

from optparse import OptionParser


parser = OptionParser(usage="""\
Send the contents of a directory as a MIME message.

Usage: %prog [options]

Unless the -o option is given, the email is sent by forwarding to your local
SMTP server, which then does the normal delivery process.  Your local machine
must be running an SMTP server.
""")
parser.add_option('-d', '--directory',
                type='string', action='store',default='.',
                help="""Mail the contents of the specified directory,
                      otherwise use the current directory.  Only the regular
                      files in the directory are sent, and we don't recurse to
                      subdirectories.""")
parser.add_option('-o', '--output',
                type='string', action='store', metavar='FILE',
                help="""Print the composed message to FILE instead of
                      sending the message to the SMTP server.""")
parser.add_option('-s', '--sender',
                type='string', action='store', metavar='SENDER',
                help='The value of the From: header (required)')
parser.add_option('-r', '--recipient',
                type='string', action='append', metavar='RECIPIENT',
                default=[], dest='recipients',
                help='A To: header value (at least one required)')
parser.add_option("-f", "--file", 
                action="store_true",
                dest="msgIsFile",
                help="Use file specified as third argument as message .", )
parser.add_option("-q", "--quiet", 
                action="store_true",
                dest="quiet",
                help="Quiet mode.", )
parser.add_option("-p", "--period",
		action="store", type="float", 
		dest="period", default=4.0,
		help="pi/period interval between samples")
opts, args = parser.parse_args()

print opts
print 'directory:',opts.directory
print 'to:',opts.recipients
print 'from:',opts.sender
print 'file:',opts.output
print parser.prog
print args

