import ConfigParser

def ReadConfig(filename):
   """ read param from config file: filename """
   rc=ConfigParser.ConfigParser()
   rc.read(filename)
   maxlen=rc.getint('global','maillen')
   smtpcount=rc.getint('global','smtp')
   defaultsmtp=rc.get('global','default')
   param=[]
   param.append([maxlen,smtpcount])
   smpts={}
   for i in xrange(1,smtpcount+1):
      sname='smtp'+'%d'%i
      key=rc.get(sname,'name')
      addr=rc.get(sname,'addr')
      port=rc.getint(sname,'port')
      ssl=rc.getint(sname,'ssl')
      username=rc.get(sname,'user')
      password=rc.get(sname,'pass')
      smpts[key]=[addr,port,ssl,username,password]
   param.append(smpts)

   return param


cf=ConfigParser.ConfigParser()
cf.read('/home/rui/.xmailrc')
cf.get("global","maillen")
cf.getint('smtp1','ssl')
cf.getboolean('smtp1','ssl')
cf.getfloat('global','maillen')
print cf.getint('global','smtp')

print ReadConfig('/home/rui/.xmailrc')
