#- * - encoding: utf-8 - * -

# module for class SMTPServer 
#
import os
import smtplib
import email,mimetypes
import smtpssl

from email import encoders
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import make_msgid

class SMTPServer:
    """ class SMTPServer is the infomation about a SMTP Server, include
        email,user:passwd@addr:port ssl,
        So you can MailBigFile, and MailFiles by it """
    def __init__(self,email='r01ustc@gmail.com',ip='localhost',port=25,ssl=0,username='',password=''):
        self.email=email
        self.addr=ip
        self.port=port
        self.ssl=ssl
        self.user=username
        self.passwd=password
    def __str__(self):
        """ override the str(), used often by print """
        if not self.ssl:
           sslmsg='Non SSL'
        elif self.ssl==1:
           sslmsg='Over SSL'
        elif self.ssl==2:
           sslmsg='Start TLS'
        else:
           sslmsg='Unknown'
        return 'E-Mail:'+self.email+'\n' \
              +'Server:'+self.addr+':%d(%s)\n'%(self.port,sslmsg) \
              +'User,Pass:'+self.user+','+self.passwd
    # info=[addr,port,ssl,username,password,mailaddress]
    def SetInfoFromList(self,info):
        """ Set Information of SMTP Server in a list """
        self.email=info[5]
        self.addr=info[0]
        self.port=info[1]
        self.ssl=info[2]
        self.user=info[3]
        self.passwd=info[4]
    def SetInfo(self,email='r01ustc@gmail.com',ip='localhost',port=25,ssl=0,username='',password=''):
        """ Set Infomation of SMTP Server """
        self.email=email
        self.addr=ip
        self.port=port
        self.ssl=ssl
        self.user=username
        self.passwd=password

    def SendMail(self,tolist,msg):
        """ Send msg to tolist. 
        tolist: the list of recipients 
           msg: formatted mail message"""
        print 'From:'+self.email
        print 'To:',tolist
        if not self.ssl:
            s=smtplib.SMTP(self.addr,self.port)
	elif self.ssl==1:
            s=smtpssl.SMTP_SSL(self.addr,self.port)
	elif self.ssl==2:
            s=smtplib.SMTP(self.addr,self.port)
            s.ehlo(self.email)
            s.starttls()
        s.ehlo(self.email)
#        if self.ssl:
#            s.starttls()
#            s.ehlo(self.email)
        s.login(self.user,self.passwd)
        s.sendmail(self.email,tolist,msg)
        s.quit()
    def MailFiles(self,tolist,subject,bodymsg, mailattachlist=[],charset=None):
        """ Mail multi files,
        tolist: the address list where we mailed to,['a@b.c','c@d.e']
        subject: the subject, used it directly. If you want to use utf-8 
                 charset, please use email.Header.Header(msg,charset) to 
                 format first
        bodymsg: the mail body, it is a list, and the first item is pure text, 
          the second item is html 
        mailattachlist is the attached file list,['a.txt','b.exe','c.jpg'] 
        charset : give the charset of mailattachlist file name"""
    
        # 创建一个multipart, 然后把前面的文本部分和html部分都附加到上面，至于为什么，可以看看mime相关内容
        attach=email.MIMEMultipart.MIMEMultipart()
#        head = email.Message.Message()
	attach['Message-ID']=make_msgid()
        attach['From']=self.email
        if type(tolist)==type([]): #if tolist is a list
            attach['To']=', '.join(tolist)
        else: # tolist is only a string
            attach['To']=tolist
        attach['Subject']=subject # email.Header.Header(subject,'utf-8')
        if type(bodymsg)==type([]): # if bodymsg is a list
            # 创建纯文本部分
            body_plain = email.MIMEText.MIMEText(bodymsg[0], _subtype='plain', _charset='utf-8')
            body_html = None
            # 创建html部分，这个是可选的
            if len(bodymsg)>1:
                body_html = email.MIMEText.MIMEText(bodymsg[1], _subtype='html', _charset='utf-8')
        else: #bodymsg is a string
            # 创建纯文本部分
            body_plain = email.MIMEText.MIMEText(bodymsg, _subtype='plain', _charset='utf-8')
            body_html = None
        attach.attach(body_plain)
        if body_html:
            attach.attach(body_html)
        # 处理每一个附件
        for fname in mailattachlist:
            ctype, encoding = mimetypes.guess_type(fname)
            if ctype is None or encoding is not None:
                # No guess could be made, or the file is encoded (compressed), so
                # use a generic bag-of-bits type.
                ctype = 'application/octet-stream'
            maintype, subtype = ctype.split('/', 1)
            filebasename=os.path.basename(fname)
            if maintype == 'text':
                fp = open(fname)
                    # Note: we should handle calculating the charset
                msg = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
#                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(fname))
            elif maintype == 'image':
                fp = open(fname, 'rb')
                msg = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
#                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(fname))
            elif maintype == 'audio':
                fp = open(fname, 'rb')
                msg = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
#                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(fname))
            else:
                fp = open(fname, 'rb')
                msg = MIMEBase(maintype, subtype)
                msg.set_payload(fp.read())
                fp.close()
                # Encode the payload using Base64
                encoders.encode_base64(msg)
                filebasename=filebasename+'.a'
            if charset: # if give the charset, we format the filename
                ffname=str(email.Header.Header(filebasename,charset))
            else: # else , just use the original name
                ffname=filebasename
            print 'Formatted filename in MailFiles:'+ffname
            msg.add_header('Content-Disposition', 'attachment', filename=ffname)
            attach.attach(msg)
        # 生成最终的邮件            
        mail = attach.as_string()
    
        self.SendMail(tolist,mail)
        return mail

    # mail part of file
    def CreateFilePartMail(self,to,subject,bodymsg,fp,filename,lbegin,lend,icount):
        """ Mail Part of the file with name filename, 
        fp is the open file
        filename is the filename, please formatted yourself first,
               You can use email.Header.Header(str,charset) to do it
        lbegin is the begin position of file
        lend is the end position of file,
        icount is the count number for the part of file, 20
        to is the address list where we mailed to,['a@b.c','c@d.e']
        subject is the subject, please formatted yourself first to support utf-8
                charset
        bodymsg is the mailbody, it is a list, and the first item is pure text, 
                            where the second item is html """
        attmsg=MIMEBase('application','octet-stream')
        fp.seek(lbegin)
        attmsg.set_payload(fp.read(lend-lbegin))
        encoders.encode_base64(attmsg)
        attmsg.add_header('Content-Disposition', 'attachment', filename=filename)
        msg=email.MIMEMultipart.MIMEMultipart()
#        head=email.Message.Message()
        msg['From']=self.email
	msg['Message-ID']=make_msgid()
        if type(to)==type([]): # if to is a list
            msg['To']=', '.join(to)
        else: # to is a single string
            msg['To']=to
        msg['Subject']=subject

        # 创建纯文本部分
        if type(bodymsg)==type([]): #bodymsg is a list
            body_plain = email.MIMEText.MIMEText(bodymsg[0], _subtype='plain', _charset='utf-8')
            msg.attach(body_plain)
            body_html = None
            # 创建html部分，这个是可选的
            if len(bodymsg)>1:
                body_html = email.MIMEText.MIMEText(bodymsg[1], _subtype='html', _charset='utf-8')
                msg.attach(body_html)
        else: # bodymsg is a single string
            body_plain = email.MIMEText.MIMEText(bodymsg, _subtype='plain', _charset='utf-8')
            msg.attach(body_plain)
        msg.attach(attmsg)

        return msg.as_string()

    # split big file to several mail
    def MailBigFile(self,tolist,subject,bodymsg,filename,leneach,ffname=None):
        """ Mail a big file by splitting it into pieces then mail,
        tolist is the address list where we mailed to,['a@b.c','c@d.e']
        subject is the subject
        bodymsg is the mailbody, it is a list, and the first item is pure text, 
            where the second item is html 
        filename is the big filename, it will appeared in body message
        leneach is the len of file piece
        ffname: formatted filename, used to give utf-8 charset name,
                you can formatted the filename by email.Header.Header(str,charset), if None, just use the basename(filename)"""

        filelen=os.stat(filename).st_size
        # lmaxpiece gives the number of file pieces
        lmaxpiece=filelen/leneach
        if(lmaxpiece*leneach < filelen):
            lmaxpiece=lmaxpiece+1
        i=0
        lbegin=0
        lend=lbegin+leneach
        fp=open(filename,'rb') # openfile
        while(i<lmaxpiece):
            if type(bodymsg)==type([]): # if body is a list
                bodymsg[0]=filename+'\n part %d/%d'%(i+1,lmaxpiece)
            else: # body is a single string
                bodymsg=filename+'\n part %d/%d'%(i+1,lmaxpiece)
            if not ffname: # if ffname=None
                ffilename=os.path.basename(filename)+'.%03d'%(i+1)
            else:
                ffilename=ffname+'.%03d'%(i+1)
            print 'formatted file name in MailBigFile:'+ffilename
            s=self.CreateFilePartMail(tolist,subject,bodymsg,fp,ffilename,lbegin,lend,i+1)
            self.SendMail(tolist,s)

            lbegin=lend
            lend=lbegin+leneach
            i=i+1

        return " "

# send mails
class MailSenders:
    """ This class is used to manage SMTPServer list """
    def __init__(self,dict,default=None):
        """ dict has the form: {name:[addr,port,ssl,username,password,mailaddress]} """
        # SMTP servers
        self.serdict=dict
        self.names=dict.keys()
        if default:
            self.current=default
        else:
            self.current=self.names[0]
        self.onlydefault=0
        self.charset=None
        self.Subject='XMailer: tool to mail multifile'

    def OnlyDefault(self,od=0):
        """ if onlydefault, we use the default SMTP Sender, else use all Servers"""
        self.onlydefault=od

    def SetCharset(self,charset=None):
        ''' set up the charset, default is None, can be like 'utf-8' to deal with chinese '''
        self.charset=charset
        
    def SetSubject(self,subj,charset=None):
        if charset:
            self.Subject=email.Header.Header(subj,charset)
        else:
            self.Subject=email.Header.Header(subj,self.charset)
        return self.Subject

    def NextServ(self):
        """ get the Next Server """
        if self.onlydefault:
            return self.current
        keylen=len(self.names)
        for i in range(0,keylen):
            if self.names[i]==self.current:
                break
        if i==keylen-1:
            i=0
        else:
            i=i+1
        self.current=self.names[i]     
        print 'Current:'+self.current
        return self.current

    def CreateBodyMail(self,bodymsg,charset=None):
        """ Create txt MIME part, listed in bodylist 
  bodylist can be a list or a single string"""
        body_html = None
        if type(bodymsg)==type([]): # if bodymsg is a list
            # 创建纯文本部分
            body_plain = email.MIMEText.MIMEText(bodymsg[0], _subtype='plain', _charset=charset)
            # 创建html部分，这个是可选的
            if len(bodymsg)>1:
                body_html = email.MIMEText.MIMEText(bodymsg[1], _subtype='html', _charset=charset)
        else: #bodymsg is a string
            # 创建纯文本部分
            body_plain = email.MIMEText.MIMEText(bodymsg, _subtype='plain', _charset=charset)
        if body_html:
            return [body_plain,body_html]
        else:
            return [body_plain]

    # MIME parts contain every file in attachlist
    def CreateFilesMail(self,attachlist=[],charset=None):
        """ create a list of MIME parts, which encode every file list in attachlist,
  charset: the charset of name in attachlist"""
        mimelist=[]
        # 处理每一个附件
        for fname in attachlist:
            ctype, encoding = mimetypes.guess_type(fname)
            if ctype is None or encoding is not None:
                # No guess could be made, or the file is encoded (compressed),
                # so use a generic bag-of-bits type.
                ctype = 'application/octet-stream'
            maintype, subtype = ctype.split('/', 1)
#            print 'MIME type:',maintype,subtype
            filebasename=os.path.basename(fname)
            if maintype == 'text':
                fp = open(fname)
                # Note: we should handle calculating the charset
                msg = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == 'image':
                fp = open(fname, 'rb')
                msg = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == 'audio':
                fp = open(fname, 'rb')
                msg = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                maintype,subtype='application','octet-stream'
                fp = open(fname, 'rb')
                msg = MIMEBase(maintype, subtype)
                msg.set_payload(fp.read())
                # Encode the payload using Base64
                encoders.encode_base64(msg)
                fp.close()
                filebasename=filebasename+'.a'
            if charset: # if give the charset, we format the filename
                ffname=str(email.Header.Header(filebasename,charset))
            else: # else , just use the original name
                ffname=filebasename
#            print 'Formatted filename in MailFiles:'+ffname
            msg.add_header('Content-Disposition', 'attachment', filename=ffname)
            mimelist.append(msg)
        return mimelist

    # MIME part contain part of file
    def CreateFilePartMail(self,fp,lbegin,lend,filename):
        """ Mail Part of the file with name filename, 
        fp is the open file
        filename is the filename, please formatted yourself first,
               You can use email.Header.Header(str,charset) to do it
        lbegin is the begin position of file
        lend is the end position of file """
        attmsg=MIMEBase('application','octet-stream')
        fp.seek(lbegin)
        attmsg.set_payload(fp.read(lend-lbegin))
        encoders.encode_base64(attmsg)
        attmsg.add_header('Content-Disposition', 'attachment', filename=filename)

        return attmsg

    def MailFiles(self,tolist,subject,bodylist,filelist,bcc=None,charset=None):
        """ mail files in filelist to tolist+bcc,
  tolist: list contains the recipients
  subject: subject of the mail, already formatted
  bodylist: list contains the message body, it is MIME part
  filelist: list contains the file we want to mail. 
  bcc: BCC fields, contains the recipients which not show in mail."""
        sender=SMTPServer()
#        print 'to',tolist,'bcc',bcc

        msg=email.MIMEMultipart.MIMEMultipart()
#        head = email.Message.Message()
        msg['Message-ID']=make_msgid()
        if type(tolist)==type([]): #if tolist is a list
            msg['To']=', '.join(tolist)
            recipients=tolist[0:]
        else: # tolist is only a string
            msg['To']=tolist
            recipients=[tolist]
        if bcc: # bcc is not empty
            if type(bcc)==type([]):
                recipients.extend(bcc)
            else:
                recipients.append(bcc)
        print 'recipients:',recipients
        msg['Subject']=subject # email.Header.Header(subject,'utf-8')
        if type(bodylist)==type([]):
            for mi in bodylist:
                msg.attach(mi)
        else:
            msg.attach(bodylist)
        flist=self.CreateFilesMail(filelist,charset)
        for fmi in flist:
            msg.attach(fmi)
        sender.SetInfoFromList(self.serdict[self.current])
        msg['From']=sender.email
        sender.SendMail(recipients,msg.as_string()) 
        self.NextServ()

    def MailBigFile(self,tolist,subject,bodylist,pathname,leneach,bcc=None,charset=None):
        """ mail a big file to tolist+bcc,
  tolist: list contains the recipients
  subject: subject formatted
  bodylist: contains the message, which maybe the infomation of the file
  pathname: full path name of the file
  leneach: len of each mail
  bcc: BCC field
  charset: charset """
        sender=SMTPServer()

        if type(tolist)==type([]): # if to is a list
            headto=', '.join(tolist)
            recipients=tolist[0:]
        else: # to is a single string
            headto=tolist
            recipients=[tolist]
        if bcc:
            if type(bcc)==type([]):
                recipients.extend(bcc)
            else:
                recipients.append(bcc)

        if charset:
            ffname=str(email.Header.Header(os.path.basename(pathname),charset))
        else:
            ffname=os.path.basename(pathname)

        filelen=os.stat(pathname).st_size
        # lmaxpiece gives the number of file pieces
        lmaxpiece=filelen/leneach
        if(lmaxpiece*leneach < filelen):
            lmaxpiece=lmaxpiece+1
        i=0
        lbegin=0
        lend=lbegin+leneach
        fp=open(pathname,'rb') # openfile
        while(i<lmaxpiece):
            ffilename=ffname+'.%03d'%(i+1)
            print 'formatted file name in MailBigFile:'+ffilename
            msg=email.MIMEMultipart.MIMEMultipart()
            if type(bodylist)==type([]):
                for msgi in bodylist:
                    msg.attach(msgi)
            else:
                msg.attach(bodylist)
            msg.attach(self.CreateFilePartMail(fp,lbegin,lend,ffilename))
            sender.SetInfoFromList(self.serdict[self.current])
            msg['To']=headto
            msg['Subject']=subject
            msg['From']=sender.email
            msg['Message-ID']=make_msgid()
            sender.SendMail(recipients,msg.as_string())
            self.NextServ()
            
            lbegin=lend
            lend=lbegin+leneach
            i=i+1

